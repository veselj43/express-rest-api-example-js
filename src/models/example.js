class Example {
    constructor(data) {
        this.id = data.id || 2;
        this.name = data.name;
    }
}

export default Example;
