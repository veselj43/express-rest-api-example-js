import Boom from 'boom';

import Example from '../models/example';

export function getExample(id) {
    if (!id) {
        return [
            {
                id: 1,
                name: 'Honza',
            },
        ];
    }

    if (+id === 1) {
        return new Example({
            id: 1,
            name: 'Honza',
        });
    }

    throw Boom.notFound('Example not found.');
}

export function create(example) {
    return new Example(example);
}

export function update(id, example) {
    return new Example({ ...example, id });
}

export function remove(id) {
    if (id) {
        return null;
    }

    throw Boom.notFound('Example not found.');
}
