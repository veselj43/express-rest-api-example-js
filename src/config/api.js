export default {
    v1: {
        api: '/api/v1',
        docs: '/docs/v1',
    },
};
