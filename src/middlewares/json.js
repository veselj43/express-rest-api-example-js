import Boom from 'boom';
import _isEmpty from 'lodash/isEmpty';

/**
 * Middleware to handle empty JSON body requests and other edge cases if any.
 */
export default function json(req, res, next) {
    const { body, method } = req;
    const disallowedHttpHeaders = ['PUT', 'POST', 'PATCH'];

    if (req.is('application/json') && disallowedHttpHeaders.includes(method) && _isEmpty(body)) {
        throw Boom.badRequest('Empty JSON');
    }

    next();
}
