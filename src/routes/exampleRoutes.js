import { Router } from 'express';

import * as exampleController from '../controllers/exampleController';
import { findExample, exampleValidator } from '../validators/exampleValidator';

/**
 * /examples
 */
const router = Router();

router.get('/', exampleController.fetch);

router.get('/:id', exampleController.fetch);

router.post('/', exampleValidator, exampleController.create);

router.put('/:id', findExample, exampleValidator, exampleController.update);

router.delete('/:id', findExample, exampleController.remove);

export default router;
