import HttpStatus from 'http-status-codes';

import * as exampleService from '../services/exampleService';

export function fetchAll(req, res, next) {
    try {
        const data = exampleService.getExample();
        res.json({ data });
    } catch (err) {
        next(err);
    }
}

export function fetch(req, res, next) {
    try {
        const data = exampleService.getExample(req.params.id);
        res.json({ data });
    } catch (err) {
        next(err);
    }
}

export function create(req, res, next) {
    try {
        const data = exampleService.create(req.body);
        res.status(HttpStatus.CREATED).json(data);
    } catch (err) {
        next(err);
    }
}

export function update(req, res, next) {
    try {
        const data = exampleService.update(req.params.id, req.body);
        res.json(data);
    } catch (err) {
        next(err);
    }
}

export function remove(req, res, next) {
    try {
        exampleService.remove(req.params.id);
        res.status(HttpStatus.NO_CONTENT).json();
    } catch (err) {
        next(err);
    }
}
