import { Router } from 'express';

import swaggerSpec from './utils/swagger';
import exampleRoutes from './routes/exampleRoutes';

/**
 * Contains all API routes for the application.
 */
const router = Router();

/**
 * GET /api/swagger.json
 */
router.get('/swagger.json', (req, res) => {
    res.json(swaggerSpec);
});

/**
 * GET /api
 */
router.get('/', (req, res) => {
    res.json({
        app: req.app.locals.title,
        apiVersion: req.app.locals.version,
    });
});

router.use('/examples', exampleRoutes);

export default router;
